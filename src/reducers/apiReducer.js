import initialState from './initialState';

import * as actions from '../actions/actions';

export default function( state = initialState.api, action) {
    let update, delegates;
    switch ( action.type ) {
        case actions.ACTION_APPROVE_DELEGATE_SUCCESS:
            update = action.data.response[2][0];
            delegates = state.get('delegates').filter(i=>i.ID !== update.ID);
            delegates.push(update);
            return state
                .set( 'delegates', delegates )
                .set( 'success', action.data.status);

        case actions.ACTION_DELETE_DELEGATE_SUCCESS:
            update = action.data.response[1][0];
            delegates = state.get('delegates').filter(i=>i.ID !== update.ID);
            return state
                .set( 'delegates', delegates )
                .set( 'success', action.data.status);

        case actions.ACTION_REJECT_DELEGATE_SUCCESS:
            update = action.data.response[2][0];
            delegates = state.get('delegates').filter(i=>i.ID !== update.ID);
            delegates.push(update);
            return state
                .set( 'delegates', delegates )
                .set( 'success', action.data.status);

        case actions.ACTION_RESET_DELEGATE_SUCCESS:
            update = action.data.response[2][0];
            delegates = state.get('delegates').filter(i=>i.ID !== update.ID);
            delegates.push(update);
            return state
                .set( 'delegates', delegates )
                .set( 'success', action.data.status);

        case actions.ACTION_PATCH_TIP_SUCCESS:
            update = action.data.response[2][0];
            delegates = state.get('delegates').filter(i=>i.ID !== update.ID);
            delegates.push(update);
            return state
                .set( 'delegates', delegates )
                .set( 'success', action.data.status);

        case actions.ACTION_FETCH_ALL_DELEGATES_SUCCESS:
            return state
                .set( 'delegates', action.data.response )
                .set( 'success', action.data.status);

        case actions.ACTION_FETCH_LATEST_APPROVED_DELEGATES_SUCCESS:
            delegates = action.data.response;//.sort(()=> .5 - Math.random());
            return state
                .set( 'delegates', delegates )
                .set( 'success', action.data.status);

        case actions.ACTION_FETCH_APPROVED_DELEGATES_SUCCESS:
            return state
                .set( 'delegates', action.data.response )
                .set( 'success', action.data.status);

        case actions.ACTION_FETCH_REJECTED_DELEGATES_SUCCESS:
            return state
                .set( 'delegates', action.data.response )
                .set( 'success', action.data.status);

        case actions.ACTION_FETCH_PENDING_DELEGATES_SUCCESS:
            return state
                .set( 'delegates', action.data.response )
                .set( 'success', action.data.status);

        case actions.ACTION_CREATE_DELEGATE_SUCCESS:
            return state
                .set( 'success', action.data.status);

        case actions.ACTION_API_ERROR:
            return state.set( 'error', action.data );

        default:
            return state;
    }
}

// return state.set( 'delegates', action.data.response );