import initialState from './initialState';

import * as actions from '../actions/actions';

export default function( state = initialState.delegate, action) {
    switch ( action.type ) {
        case actions.ACTION_UPDATE_DATAURI:
            return state.set( 'dataURI', action.data );
        case actions.ACTION_UPDATE_TIP:
            return state.set( 'tip', action.data );
        case actions.ACTION_UPDATE_FIRST:
            return state.set( 'first', action.data );
        case actions.ACTION_UPDATE_LAST:
            return state.set( 'last', action.data );
        case actions.ACTION_UPDATE_EMAIL:
            return state.set( 'email', action.data );
        case actions.ACTION_UPDATE_CONTACT_CONSENT:
            return state.set( 'contactConsent', action.data );
        case actions.ACTION_UPDATE_PROJECT_CONSENT:
            return state.set( 'projectConsent', action.data );
        case actions.ACTION_UPDATE_SIGNATURE:
            return state.set( 'signature', action.data );
        case actions.ACTION_UPDATE_DATE:
            return state.set( 'date', action.data );
        case actions.ACTION_UPDATE_PHOTO_CONSENT:
            return state.set( 'photoConsent', action.data );
        default:
            return state;
    }
}
