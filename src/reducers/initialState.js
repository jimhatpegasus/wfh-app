import { fromJS } from 'immutable';

export default {
    delegate: fromJS({
        dataURI: '',
        tip: '',
        first: '',
        last: '',
        email: '',
        contactConsent: false,
        projectConsent: false,
        signature: '',
        date: '',
        photoConsent: false
    }),
    api: fromJS({
        success: '',
        error: '',
        delegates: []
    }),
    busy: fromJS({
        open: false
    }),
    modal: fromJS({
        open: false,
        mode: '',
        contents: ''
    }),
};