import { combineReducers } from 'redux';

import delegate from './delegateReducer';
import api from './apiReducer';
import modal from './modalReducer';

const rootReducer = combineReducers({
    delegate,
    api,
    modal
});

export default rootReducer;