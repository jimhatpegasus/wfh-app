import initialState from './initialState';

import * as actions from '../actions/actions';

export default function( state = initialState.modal, action) {
    switch ( action.type ) {
        case actions.ACTION_OPEN_MODAL:
            return state.merge({
                open: true,
                mode: action.data.mode,
                contents: action.data.contents
            });
        case actions.ACTION_CLOSE_MODAL:
            return state.set('open', false);
        default:
            return state;
    }
}