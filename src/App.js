import React, { Component } from 'react';
import {  BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import './App.css';

import Moderation from './views/Moderation';
import PhotoBooth from './views/PhotoBooth';
import AutoGallery from './views/AutoGallery';
import TouchGallery from './views/TouchGallery';
import Banner from './views/Banner';

import Modal from './views/Modal';
import Busy from './views/Busy';

import {
    closeModal,
    openModal,
    openBusy
} from './actions/modalActions';

import {
    updateTip
} from './actions/dataActions';

import {
    MODAL_TEXT,
    MODAL_IMAGE,
    MODAL_ERROR,
    MODAL_ALERT
} from './views/Constants';

class App extends Component {

    constructor(props) {
        super(props);
        this.openImageInModal = this.openImageInModal.bind(this);
        this.openTextInModal = this.openTextInModal.bind(this);
        this.openAlertInModal = this.openAlertInModal.bind(this);
        this.openBusy = this.openBusy.bind(this);
        this.editMessage = this.editMessage.bind(this);
    }

    openAlertInModal() {
        this.props.openModal({
            mode: MODAL_ALERT,
            contents: 'New delegate(s)'
        });
    }

    openBusy() {
        this.props.openBusy({
            mode: '',
            contents: ''
        });
    }

    editMessage(e) {
        e.preventDefault();
        this.props.updateTip(e.target.getAttribute('data-id'), e.target.getAttribute('data-message'));
    }

    openImageInModal(e) {
        e.preventDefault();
        this.props.openModal({
            mode: MODAL_IMAGE,
            contents: e.target.getAttribute('data-contents')
        });
    }

    openTextInModal(e) {
        e.preventDefault();
        this.props.openModal({
            mode: MODAL_TEXT,
            contents: {'message': e.target.getAttribute('data-contents'), 'id': e.target.getAttribute('data-id')}
        });
    }

    componentWillReceiveProps( nextProps ) {
        if (this.props.api.get('error') !== nextProps.api.get('error')) {
            const resp = nextProps.api.get('error');
            let status = '', statusText = '', errorCode = '';
            console.log(resp);
            if (resp.status) {
                status = resp.status;
                if (resp.error) {
                    if (resp.error.code) {
                        errorCode = resp.error.code
                    }
                }
            }
            this.props.openModal({
                mode: MODAL_ERROR,
                contents: `${status} ${statusText} ${errorCode}`
            });
        }
    }

    render() {
        return (
            <main>
                <Modal editMessage={this.editMessage} show={this.props.modal.get('open')} close={this.props.closeModal} mode={this.props.modal.get('mode')} contents={this.props.modal.get('contents')}/>
                {/*<Busy show={this.props.busy.get('open')}/>*/}
                <Router>
                    <Switch>
                        <Route exact path="/" component={PhotoBooth} />
                        <Route exact path="/photobooth" component={PhotoBooth} />
                        <Route exact path="/moderation" render={(props) => (
                            <Moderation openBusy={this.openBusy} openAlertInModal={this.openAlertInModal} openImageInModal={this.openImageInModal} openTextInModal={this.openTextInModal} />
                        )}/>
                        <Route exact path="/autogallery" component={AutoGallery} />
                        <Route exact path="/touchgallery" component={TouchGallery} />
                        <Route exact path="/banner" component={Banner} />
                    </Switch>
                </Router>
            </main>
        )
    }
}

function mapStateToProps( state ) {
    return {
        modal: state.modal,
        busy: state.busy,
        api: state.api
    };
}

function mapDispatchToProps( dispatch ) {
    return bindActionCreators( {
        closeModal: closeModal,
        openModal: openModal,
        openBusy: openBusy,
        updateTip: updateTip
    }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( App );
