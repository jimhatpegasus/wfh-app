import React, { Component } from 'react';

import {
    IMAGE_PATH
} from './Constants';

class Delegate extends Component {

    submitModeration(value, e) {
        switch (value) {
          case 'approve':
            this.props.approve(e);
            break;
          case 'reject':
            this.props.reject(e);
            break;
          case 'reset':
            this.props.reset(e);
            break;
          case 'deleteDelegate':
            this.props.deleteDelegate(e);
            break;
        };
        
        this.props.onModerationClicked();
    }

    render() {

        const { delegate, deleteDelegate, approve, reject, reset, openImageInModal, openTextInModal, isButtonDisabled } = this.props;

        const status =
            delegate.approved ?
                <span className='approved'>approved</span> :
                delegate.rejected ?
                    <span className='rejected'>rejected</span> :
                    delegate.pending ?
                        <span className='pending'>pending</span> : '';

        const moderate =
            <form>
                <button data-id={delegate.ID} onClick={this.submitModeration.bind(this, "approve")} disabled={isButtonDisabled} type="button" className="small success button">Approve</button>
                <button data-id={delegate.ID} onClick={this.submitModeration.bind(this, "reject")} disabled={isButtonDisabled} type="button" className="small warning button">Reject</button>
                <button data-id={delegate.ID} onClick={this.submitModeration.bind(this, "deleteDelegate")} disabled={isButtonDisabled} type="button" className="small alert button">Delete</button>
            </form>;

        const resetable =
            <form>
                <button data-id={delegate.ID} onClick={this.submitModeration.bind(this, "reset")} disabled={isButtonDisabled} type="button" className="small button">Reset</button>
                <button data-id={delegate.ID} onClick={this.submitModeration.bind(this, "deleteDelegate")} disabled={isButtonDisabled} type="button" className="small alert button">Delete</button>
            </form>

        const controls =
            delegate.approved || delegate.rejected ?
                resetable :
                delegate.pending ?
                    moderate : '';

        return (
            <tr className='delegate'>
                <td className='name'>
                    <img data-contents={delegate.file} onClick={openImageInModal} alt='' title={`${delegate.file}`} width='32px' height='32px' src={`${IMAGE_PATH}${delegate.file}`}/>
                    {delegate.name}&nbsp;{delegate.last}{/*<span style={{'color':'lightgray'}}>&nbsp;:{delegate.ID}</span>*/}
                </td>
                <td>{delegate.email}</td>
                <td data-id={delegate.ID} data-contents={delegate.message} onClick={openTextInModal} className='message'>{delegate.message}</td>
                <td>{status}</td>
                <td>{controls}</td>
            </tr>
        )
    }
}

// <img data-mode={MODAL_IMAGE} data-contents={delegate.file} onClick={openModal} alt='' title={`${delegate.file}`} width='32px' height='32px' src={`${IMAGE_PATH}${delegate.file}`}/>
// <td data-mode={MODAL_TEXT} data-contents={delegate.message} onClick={openModal} className='message'>{delegate.message}</td>
export default Delegate;