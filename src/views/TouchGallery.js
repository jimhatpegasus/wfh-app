import React, { Component } from 'react';

import { connect } from 'react-redux';

import {
    fetchApprovedDelegates
} from '../actions/dataActions';

import {
    NEW_APPROVAL,
    APPROVAL_REVOKED,
    DELEGATE_DELETED,
    TIP_UPDATED,
    IMAGE_PATH
} from "./Constants";

import { bindActionCreators } from 'redux';

import { subscribeToSocket } from './socket';

import ShowId from './ShowId';

class TouchGallery extends Component {

    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            totalPages: 1,
            active: -1
        };
        document.body.classList.remove('mobile');
        this.approvedReady = this.approvedReady.bind(this);
        this.previousPage = this.previousPage.bind(this);
        this.nextPage = this.nextPage.bind(this);
        this.showTip = this.showTip.bind(this);
        this.closeTip = this.closeTip.bind(this);
        this.props.fetchApprovedDelegates();
        subscribeToSocket(this.approvedReady);
        this.z = 99;
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.api.get('delegates').length !== this.props.api.get('delegates').length) {
            this.setState({
                totalPages: parseInt(((this.props.api.get('delegates').length - 1) / 12) + 1, 10)
            })
        }
    }

    showTip(e) {
        e.preventDefault();
        const id = parseInt(e.target.getAttribute('data-id'), 10);
        this.setState({
            active: id
        }, ()=> {
            document.querySelectorAll(`[data-id='${id}']`)[0].parentNode.parentNode.style.zIndex = this.z++;
        });
    }

    closeTip(e) {
        e.preventDefault();
        this.setState({
            active: -1
        })
    }

    approvedReady(a, res){
        if (res.error) {
            console.log(res);
        } else {
            if (res.type === NEW_APPROVAL || res.type === APPROVAL_REVOKED || res.type === DELEGATE_DELETED || res.type === TIP_UPDATED) {
                this.props.fetchApprovedDelegates();
            }
        }
    }

    nextPage(e){
        e.preventDefault();
        this.setState({
            page: this.state.page + 1,
            active: -1
        })
    }

    previousPage(e){
        e.preventDefault();
        this.setState({
            page: this.state.page - 1,
            active: -1
        })
    }

    render() {

        const delegates = this.props.api.get('delegates').sort((a, b) => b.ID - a.ID);

        let grouped = [], c = 0;

        do {
            grouped.push(delegates.slice(c, c + 12));
            c = c + 12;
        }
        while ( c < delegates.length + 0 );

        const positionStyle = { 'transition': '1s ease-in-out', 'transform': `translate3d(0,-${this.state.page - 1}00vh,0)`};

        return (
            <main className='touch-gallery'>
                <nav>
                    <img src={require('../images/faces-logo.png')} alt='' id='faces-logo'/>
                    <img src={require('../images/csl-logo.png')} alt='' id='csl-logo'/>
                </nav>

                <div className='gallery'>
                    <div className='gallery-container' style={positionStyle}>
                        {
                            Object.keys(grouped).map((g, i) => {
                                const top = {'top': ((i * 100) + 8) + '%'};
                                return <div key={`g-${i}`} style={top} className={`gallery-grid`}>
                                    {
                                        grouped[g].map((d, j) => {
                                            return <div key={`b-${j}`} className={`delegate ${this.state.active === d.ID ? 'showing-tip no-filter' : this.state.active > -1 ? 'filter' : ''}`}>
                                                <div className='container'>
                                                    <a className='photograph' data-id={d.ID} onClick={this.showTip} style={{'backgroundImage' : `url(${IMAGE_PATH}${d.file})`}}>&nbsp;<ShowId ID={d.ID}/></a>
                                                    <div className='tip'>
                                                        <p>{d.message}</p>
                                                    </div>
                                                    <span data-id={d.ID} onClick={this.closeTip}  className='close'>close</span>
                                                </div>
                                            </div>
                                            })
                                        }
                                </div>}
                            )
                        }
                    </div>
                </div>

                <footer>
                    <p>Use the up and down arrows to view more photos and messages</p>
                    <p>Tap photos to reveal the message provided by the delegate pictured
                        <span className='job-code'>UK/COAG/18-0046b   Date of preparation: April 2018</span>
                    </p>
                    <div className='buttonslider'>
                        <div onClick={this.previousPage} className={`chevron white up ${this.state.page === 1 ? 'disabled' : ''}`}></div>
                        <div onClick={this.nextPage}  className={`chevron white down ${this.state.page === this.state.totalPages ? 'disabled' : ''}`}></div>
                    </div>
                </footer>
            </main>

        )
    }
}


function mapStateToProps( state ) {
    return {
        api: state.api
    };
}

function mapDispatchToProps( dispatch ) {
    return bindActionCreators( {
        fetchApprovedDelegates: fetchApprovedDelegates
    }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( TouchGallery );

