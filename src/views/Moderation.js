import React, { Component } from 'react';

import Chance from 'chance';
import loremIpsum from 'lorem-ipsum';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { subscribeToSocket } from './socket';

import DelegatesList from './DelegatesList';

import {
    ALL,
    APPROVED,
    REJECTED,
    PENDING,
    IMAGE_READY
} from './Constants';

import {
    approveDelegate,
    deleteDelegate,
    rejectDelegate,
    resetDelegate,
    createDelegate,
    fetchApprovedDelegates,
    fetchRejectedDelegates,
    fetchPendingDelegates,
    fetchAllDelegates
} from '../actions/dataActions';

class Moderation extends Component {

    constructor(props) {
        super(props);
        this.onModerationClicked = this.onModerationClicked.bind(this);
        this.state = {
            isButtonDisabled: false,
            createDelegateOpen: false,
            filter: ALL,
            form: {
                name: '',
                email: '',
                message: '',
                file: ''
            }/*,
            modal: {
                show: false,
                mode: null,
                content: null
            }*/
        };
        this.approve = this.approve.bind(this);
        this.reject = this.reject.bind(this);
        this.reset = this.reset.bind(this);
        this.delete = this.delete.bind(this);
        this.updateDelegates = this.updateDelegates.bind(this);
        this.filter = this.filter.bind(this);
        this.createDelegate = this.createDelegate.bind(this);
        this.changeName = this.changeName.bind(this);
        this.changeEmail = this.changeEmail.bind(this);
        this.changeMessage = this.changeMessage.bind(this);
        this.imageReady = this.imageReady.bind(this);
        this.fileChange = this.fileChange.bind(this);
        this.showCreateDelegate = this.showCreateDelegate.bind(this);

        document.body.classList.remove('mobile');
        subscribeToSocket(this.imageReady);
        document.body.classList.add('cream');
    }

    fileChange(e) {
        e.preventDefault();
        const file = e.target.files[0];
        if(file) {
            let reader = new FileReader();
            const _this = this;
            reader.onload = function(e) {
                const form = {..._this.state.form};
                form.file = e.target.result;
                _this.setState({form});
            };
            reader.readAsDataURL(file);
        } else {
            const form = {...this.state.form};
            form.file = '';
            this.setState({form})
        }
    }

    imageReady(a, res){
        if (res.error) {
            console.log(res);
        } else {
            if (res.type === IMAGE_READY) {
                /* this.setState({
                    filter: PENDING
                }); */
                this.updateDelegates();
                this.props.openAlertInModal();
            }
        }
    }

    showCreateDelegate(e) {
        e.preventDefault();
        this.setState({
            createDelegateOpen: !this.state.createDelegateOpen
        })
    }

    approve(e) {
        e.preventDefault();
        console.log('Approved called');
        this.props.openBusy();
        this.props.approveDelegate(e.target.getAttribute('data-id'));
    }

    delete(e) {
        e.preventDefault();
        //this.props.openBusyInModal();
        this.props.deleteDelegate(e.target.getAttribute('data-id'));
    }

    reject(e) {
        e.preventDefault();
        //this.props.openBusyInModal();
        this.props.rejectDelegate(e.target.getAttribute('data-id'));
    }

    reset(e) {
        e.preventDefault();
        //this.props.openBusyInModal();
        this.props.resetDelegate(e.target.getAttribute('data-id'));
    }

    updateDelegates() {
        //this.props.openBusyInModal();
        switch(this.state.filter) {
            case ALL:
                this.props.fetchAllDelegates();
                break;
            case APPROVED:
                this.props.fetchApprovedDelegates();
                break;
            case REJECTED:
                this.props.fetchRejectedDelegates();
                break;
            case PENDING:
                this.props.fetchPendingDelegates();
                break;
            default:
                this.props.fetchAllDelegates();
        }
    }

    onModerationClicked () {
        console.log('Disable button for three seconds');
        this.setState({
            isButtonDisabled: true
        });

        setTimeout(() => this.setState({ isButtonDisabled: false }), 3000);
    }


    filter(e){
        e.preventDefault();
        this.setState({
            filter: e.target.value
        }, () => {
            this.updateDelegates()
        });
    }

    createDelegate(e) {
        e.preventDefault();
        //this.props.openBusyInModal();
        this.props.createDelegate({
            first: this.state.form.name,
            last: '',
            email: this.state.form.email,
            message: this.state.form.message,
            dataURI: this.state.form.file
        });
    }

    changeName(e) {
        e.preventDefault();
        const form = {...this.state.form};
        form.name = e.target.value;
        this.setState({form})
    }

    changeEmail(e) {
        e.preventDefault();
        const form = {...this.state.form};
        form.email = e.target.value;
        this.setState({form})
    }

    changeMessage(e) {
        e.preventDefault();
        const form = {...this.state.form};
        form.message = e.target.value;
        this.setState({form})
    }

    componentDidMount() {
        this.updateDelegates();
    }

    render() {

        const { openImageInModal, openTextInModal } = this.props;

        //const disabled = this.state.form.file === '' || this.state.form.name === '' || this.state.form.email === '' || this.state.form.email.search(/@/) === -1 || this.state.form.email.search(/\./) === -1 || this.state.form.message === '';

        //const createDelegateOpenStyle = this.state.createDelegateOpen ? {'display': 'block'} : {'display': 'none'};
        //const collapseExpandArrow = this.state.createDelegateOpen ? 'arrow-down' : 'arrow-right';

        return (
            <main className='moderation'>
                {/*<header>
                    <img alt='' src={require('../images/csl-logo.png')} className='csl-logo'/>
                </header>*/}
                <nav>
                    <img src={require('../images/faces-logo.png')} alt='' id='faces-logo'/>
                    <img src={require('../images/csl-logo.png')} alt='' id='csl-logo'/>
                </nav>
                <div className='contents'>
                    {/*<h3>Moderation</h3>*/}
                    <form>
                        Filter&nbsp;
                        <select onChange={this.filter} value={this.state.filter}>
                            <option value={ALL}>All</option>
                            <option value={APPROVED}>Approved</option>
                            <option value={REJECTED}>Rejected</option>
                            <option value={PENDING}>Pending</option>
                        </select>
                    </form>
                    <DelegatesList openImageInModal={openImageInModal} openTextInModal={openTextInModal} deleteDelegate={this.delete} approve={this.approve} reject={this.reject} reset={this.reset} delegates={this.props.api.get('delegates')} onModerationClicked={this.onModerationClicked} isButtonDisabled={this.state.isButtonDisabled} ></DelegatesList>
                    <div className='job-code-container'>
                        <span className='job-code'>UK/COAG/18-0046a   Date of preparation: April 2018</span>
                    </div>
                </div>
            </main>
        )
    }
}

function mapStateToProps( state ) {
    return {
        api: state.api
    };
}

function mapDispatchToProps( dispatch ) {
    return bindActionCreators( {
        approveDelegate: approveDelegate,
        deleteDelegate: deleteDelegate,
        rejectDelegate: rejectDelegate,
        resetDelegate: resetDelegate,
        createDelegate: createDelegate,
        fetchAllDelegates: fetchAllDelegates,
        fetchApprovedDelegates: fetchApprovedDelegates,
        fetchRejectedDelegates: fetchRejectedDelegates,
        fetchPendingDelegates: fetchPendingDelegates
    }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( Moderation );
