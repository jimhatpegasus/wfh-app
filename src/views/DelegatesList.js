import React, { Component } from 'react';

import Delegate from './Delegate';

class DelegatesList extends Component {
    render() {

        const { delegates, approve, deleteDelegate, reject, reset, openImageInModal, openTextInModal, onModerationClicked, isButtonDisabled } = this.props;

        const none = delegates.length === 0 ? <tr><td>Nothing to show</td></tr> : null;

        delegates.sort((a, b) => b.ID - a.ID);

        return (
            <div className='delegate-list'>
                <table>
                    <thead>
                        <tr>
                            <td>Name<br/>
                                <small><em>Click picture to zoom</em></small>
                            </td>
                            <td>Email</td>
                            <td>Message<br/>
                                <small><em>Click text for full details</em></small>
                            </td>
                            <td className='status'>Status</td>
                            <td className='actions-column'>Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        delegates.map(d => <Delegate openImageInModal={openImageInModal} openTextInModal={openTextInModal} key={d.ID} delegate={d} deleteDelegate={deleteDelegate} approve={approve} reject={reject} reset={reset} onModerationClicked={onModerationClicked} isButtonDisabled={isButtonDisabled} />)
                    }
                    {none}
                    </tbody>
                </table>
            </div>
        )
    }
}

// <Delegate openModal={/*openModal*/} key={d.ID} delegate={d} deleteDelegate={deleteDelegate} approve={approve} reject={reject} reset={reset}/>

export default DelegatesList;