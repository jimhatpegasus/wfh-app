export const ALL = 'app/ALL';
export const APPROVED = 'app/APPROVED';
export const REJECTED = 'app/REJECTED';
export const PENDING = 'app/PENDING';

export const IMAGE_READY = 'app/IMAGE_READY';
export const IMAGE_ERROR = 'app/IMAGE_ERROR';

export const NEW_APPROVAL = 'app/NEW_APPROVAL';
export const APPROVAL_REVOKED = 'app/APPROVAL_REVOKED';
export const DELEGATE_DELETED = 'app/DELEGATE_DELETED';
export const TIP_UPDATED = 'app/TIP_UPDATED';

export const MODAL_IMAGE = 'app/MODAL_IMAGE';
export const MODAL_TEXT = 'app/MODAL_TEXT';
export const MODAL_ERROR = 'app/MODAL_ERROR';
export const MODAL_ALERT = 'app/MODAL_ALERT';

export const SUBSCRIBE_TO_SOCKET = 'app/SUBSCRIBE_TO_SOCKET';

export const WELCOME = 'app/WELCOME';
export const CAMERA = 'app/CAMERA';
export const TIP = 'app/TIP';
export const CONSENT = 'app/CONSENT';
export const THANKS = 'app/THANKS';

export const SHOW_IMAGES = 'app/SHOW_IMAGES';
export const SHOW_TIPS = 'app/SHOW_TIPS';
export const SHOW_SPLASH = 'app/SHOW_SPLASH';

/* export */const CAMERA_CONSTRAINTS_IPAD = {
    video: {
        mandatory: {
            width: { min: 640 },
            height: { min: 480 }
        },
        optional: [
            { width: 640 },
            { width: { min: 640 }},
            { frameRate: 60 },
            { width: { max: 800 }},
            { facingMode: "user" }
        ]
    }
};

/* export */const CAMERA_CONSTRAINTS_DEV = {
    video: {
        width: { min: 1280 },
        height: { min: 720 }
    }
};

export const AUTO_GALLERY_TIMER = 10000;
//export const AUTO_GALLERY_REFRESH = 60000;
export const AUTO_GALLERY_SPLASH = 30000;
export const PHOTO_BOOTH_REFRESH = 15000;

const local = window.location.href.search(/localhost/) > -1;

export const CAMERA_CONSTRAINTS = local ? CAMERA_CONSTRAINTS_DEV : CAMERA_CONSTRAINTS_IPAD;

//console.log(CAMERA_CONSTRAINTS);

export const SOCKET_PATH = local ? 'http://localhost:4001' : 'https://cslmay18.pegasusstaging.com';
export const API_PATH = local ? 'http://localhost:3000/api/v1/delegate' : 'https://cslmay18.pegasusstaging.com/api/v1/delegate';
export const IMAGE_PATH = local ? 'http://localhost:3000/images/' : 'https://cslmay18.pegasusstaging.com/wfh-api/public/images/';