import React, { Component } from 'react';

import { connect } from 'react-redux';
import {
    IMAGE_PATH,
    SHOW_IMAGES,
    SHOW_TIPS,
    AUTO_GALLERY_SPLASH,
    AUTO_GALLERY_TIMER,
    //AUTO_GALLERY_REFRESH
} from "./Constants";

import {
    fetchLatestApprovedDelegates
} from '../actions/dataActions';

import { bindActionCreators } from 'redux';

import ShowId from './ShowId';

class AutoGallery extends Component {

    constructor(props) {
        super(props);
        this.state = {
            mode: SHOW_IMAGES,
            splash: true
        };
        document.body.classList.remove('mobile');
    }

    componentDidMount() {
        setInterval(()=> {
            if (this.state.mode === SHOW_IMAGES) {
                this.setState( {mode: SHOW_TIPS} );
                //this.props.fetchLatestApprovedDelegates();
            } else {
                if (this.state.mode === SHOW_TIPS) {
                    this.setState( {mode: SHOW_IMAGES} );
                    this.props.fetchLatestApprovedDelegates();
                }
            }
        }, AUTO_GALLERY_TIMER);
        /* setInterval( ()=> {
           this.props.fetchLatestApprovedDelegates();
        }, AUTO_GALLERY_REFRESH); */
        setInterval( ()=> {
            let newSplash = !this.state.splash;
            this.setState({
                splash: newSplash
            })
        }, AUTO_GALLERY_SPLASH);

        this.props.fetchLatestApprovedDelegates();
    }

    render() {

        const delegates = this.props.api.get('delegates');//.sort((a, b) => /*0.5 - Math.random()*/b.ID - a.ID);

        const displaySplash = this.state.splash ? {'transition': '0s 20s','opacity': '1'} : {'transition': '0s 2s','opacity': '0'};
        //const displayGallery = this.state.splash === false ? {'display': 'block'} : {'display': 'none'};

        return (

            <div className='auto-gallery'>
                <main style={displaySplash} className='welcome'>
                    <div className='contents'>
                        <img alt='' src={require('../images/faces-logo.png')} className='faces-logo'/>
                        <div className='border-content'>
                            <p>CSL Behring is inviting the hemophilia community to share tips and advice with other people living with<br/>the condition.</p>
                            <p>The more advice we can share, the more people<br/>with hemophilia can benefit.</p>
                        </div>
                    </div>
                    <span className='job-code'>UK/COAG/18-0046d    Date of preparation: April 2018</span>
                </main>
                <div className='auto-grid'>
                    {
                        delegates.map((d, i) => {
                            const isFeatured = (i + 1) < 3;
                            const showTips = this.state.mode === SHOW_TIPS;
                            return (<div key={`b-${i}`} className={`delegate ${isFeatured && showTips ? 'showing-tip no-filter' : showTips === false ? 'no-filter' : 'filter'} `}>
                                <div className='container'>
                                    <div className='photograph' style={{'backgroundImage' : `url(${IMAGE_PATH}${d.file})`}}>&nbsp;<ShowId ID={d.ID}/></div>
                                    <div className='tip'>
                                        <p>{d.message}</p>
                                    </div>
                                </div>
                            </div>)}
                        )
                    }
                </div>
            </div>

        )
    }
}

function mapStateToProps( state ) {
    return {
        api: state.api
    };
}

function mapDispatchToProps( dispatch ) {
    return bindActionCreators( {
        fetchLatestApprovedDelegates: fetchLatestApprovedDelegates
    }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( AutoGallery );