import React, { Component } from 'react';

import loremIpsum from 'lorem-ipsum';
import moment from 'moment';
import Chance from 'chance';

import { base64_example } from '../images/base64_example';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
    updateDataURI,
    updateTip,
    updateFirst,
    updateLast,
    updateEmail,
    updateContactConsent,
    updateProjectConsent,
    updateSignature,
    updateDate,
    updatePhotoConsent
} from '../actions/wfhActions';

import {
    createDelegate
} from '../actions/dataActions';

import {
    WELCOME,
    CAMERA,
    TIP,
    CONSENT,
    THANKS,
    CAMERA_CONSTRAINTS,
    //CAMERA_CONSTRAINTS_DEV,
    API_PATH,
    PHOTO_BOOTH_REFRESH
} from './Constants';

class PhotoBooth extends Component {

    constructor(props) {
        super(props);
        this.state = {
            step: WELCOME,
            camera: {
                counting: false,
                count: 3,
                counter: null,
                initiated: false,
                showInstructions: true,
                attempts: 0
            }
        };
        this.setStep = this.setStep.bind(this);
        this.retake = this.retake.bind(this);
        this.initCountdown = this.initCountdown.bind(this);
        this.captureImage = this.captureImage.bind(this);
        this.updateTip = this.updateTip.bind(this);
        this.updateFirst = this.updateFirst.bind(this);
        this.updateLast = this.updateLast.bind(this);
        this.updateEmail = this.updateEmail.bind(this);
        this.toggleContactConsent = this.toggleContactConsent.bind(this);
        this.toggleProjectConsent = this.toggleProjectConsent.bind(this);
        this.updateSignature = this.updateSignature.bind(this);
        this.updateDate = this.updateDate.bind(this);
        this.togglePhotoConsent = this.togglePhotoConsent.bind(this);
        this.skip = this.skip.bind(this);
        document.body.classList.add('mobile');
    }

    skip(e) { e.preventDefault(); }
    updateTip(e) { e.preventDefault(); this.props.updateTip(e.target.value); }
    updateFirst(e) { e.preventDefault(); this.props.updateFirst(e.target.value); }
    updateLast(e) { e.preventDefault(); this.props.updateLast(e.target.value); }
    updateEmail(e) { e.preventDefault(); this.props.updateEmail(e.target.value); }
    toggleContactConsent(e) { this.props.updateContactConsent(!this.props.delegate.get('contactConsent')); }
    toggleProjectConsent(e) { this.props.updateProjectConsent(!this.props.delegate.get('projectConsent')); }
    updateSignature(e) { e.preventDefault(); this.props.updateSignature(e.target.value); }
    updateDate(e) { e.preventDefault(); this.props.updateDate(e.target.value); }
    togglePhotoConsent(e) { this.props.updatePhotoConsent(!this.props.delegate.get('photoConsent')); }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.step === WELCOME && this.state.step === CAMERA && this.state.camera.initiated === false) {
            const video = document.querySelector('#video-stream');
            let camera = {...this.state.camera};
            camera.initiated = true;
            this.setState({camera});
            navigator.mediaDevices.getUserMedia(CAMERA_CONSTRAINTS).then( (stream) => video.srcObject = stream ).catch( (error) => console.log(error) );
            setTimeout(()=>this.initCountdown(), 2000);
        }
        if (prevState.step === CONSENT && this.state.step === THANKS) {
            this.props.createDelegate(this.props.delegate.toJS());
            let camera = {...this.state.camera};
            camera.counting = false;
            camera.count = 3;
            camera.counter = null;
            camera.initiated = false;
            camera.showInstructions = true;
            camera.attempts = 0;
            this.setState({camera});
            this.props.updateDataURI('');
            this.props.updateTip('');
            this.props.updateFirst('');
            this.props.updateLast('');
            this.props.updateEmail('');
            this.props.updateContactConsent(false);
            this.props.updateProjectConsent(false);
            this.props.updateSignature('');
            this.props.updateDate('');
            this.props.updatePhotoConsent(false);
            setTimeout(()=>{
                this.setState({
                    step: WELCOME
                })
            }, PHOTO_BOOTH_REFRESH);
        }
    }

    setStep(e) {
        e.preventDefault();
        this.setState({
            step: e.target.getAttribute('data-step')
        })
    }

    retake(e) {
        e.preventDefault();
        let camera = {...this.state.camera};
        camera.showInstructions = true;
        this.setState({camera});
        setTimeout(()=>this.initCountdown(), 2000);
    }

    captureImage() {
        const canvas = document.createElement('canvas');
        const video = document.querySelector('#video-stream');
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        canvas.getContext('2d').drawImage(video, 0, 0);
        let camera = {...this.state.camera};
        camera.attempts = camera.attempts + 1;
        this.setState({camera});
        this.props.updateDataURI(canvas.toDataURL('image/png'));
    }

    initCountdown() {
        let camera = {...this.state.camera};
        camera.count = 3;
        camera.counting = true;
        camera.showInstructions = false;
        camera.counter = setInterval(()=>{
            let c = this.state.camera.count;
            c = c - 1;
            let camera = {...this.state.camera};
            if (c === 0) {
                clearInterval(this.state.camera.counter);
                camera.counter = null;
                camera.counting = false;
                this.setState({camera});
                this.captureImage();
            } else {
                camera.count = c;
                this.setState({camera});
            }
        }, 1000);
        this.setState({camera});
    }

    render() {

        const displayWhenCounting = {
            'display': this.state.camera.counting ? 'block' : 'none'
        };

        const hideWhenCounting = {
            'display': this.state.camera.counting === false ? 'block' : 'none'
        };

        const hideWhenCountingOrDisplayingInstructions = {
            'display': this.state.camera.counting || this.state.camera.showInstructions ? 'none' : 'block'
        };

        const displayInstructions = {
            'display': this.state.camera.showInstructions ? 'block' : 'none'
        };

        const tipDisabled = this.props.delegate.get('tip') === '';

        const isEmpty = (key) => this.props.delegate.get(key) === '';
        const isFalse = (key) => this.props.delegate.get(key) === false;

        const consentDisabled = isEmpty('date') || isEmpty('first') || isEmpty('last') || isEmpty('signature') || /*isEmpty('email') ||*/ isFalse('photoConsent');

        return (
            <div className='photobooth'>
                {(() => {
                    switch(this.state.step) {
                        case WELCOME:
                            return (
                                <main className='photobooth welcome'>
                                    <header>
                                        <img alt='' src={require('../images/csl-logo.png')} className='csl-logo'/>
                                    </header>
                                    <div className='contents'>
                                        <img alt='' src={require('../images/faces-logo.png')} className='faces-logo'/>
                                        <h1>Take a photo and share a tip<br/>with the hemophilia community</h1>
                                        <div className='border-content'>
                                            <p>Help us build a real-life picture of hemophilia by sharing your tips and
                                                advice.</p>
                                            <p>Tips can cover anything about life with hemophilia that you feel would be
                                                useful to share with patients, family members or carers.</p>
                                            <p>Once submitted, both your tip and photo will appear on the screen on our
                                                stand.</p>
                                            <p>The more advice we can share, the more people with hemophilia can
                                                benefit.</p>
                                            <p>In addition, CSL Behring will make a donation to The Haemophilia Society
                                                UK.</p>
                                        </div>
                                    </div>
                                    <footer>
                                        <a className='cta-bottom' data-step={CAMERA} onClick={this.setStep}>Take my photo</a>
                                        {/*<div onClick={this.skip} className='skip'><span className='chevron thick right'></span></div>*/}
                                    </footer>
                                </main>
                            );
                        case CAMERA:
                            return (
                                <main className='photobooth camera'>
                                    <header>
                                        <img alt='' src={require('../images/csl-logo.png')} className='csl-logo'/>
                                    </header>
                                    <video id='video-stream' className='video-stream' autoPlay></video>
                                    <img src={this.props.delegate.get('dataURI')} style={hideWhenCounting} alt='' id='photo-capture' className='photo-capture'/>
                                    <div className='portrait-guide'></div>
                                    {
                                        this.state.camera.attempts < 4 ? <button style={hideWhenCountingOrDisplayingInstructions} onClick={this.retake} className='retake'><span className='chevron left'>Re-take photo</span></button> : ''
                                    }
                                    {
                                        this.state.camera.attempts < 4 ? <div style={hideWhenCountingOrDisplayingInstructions} className='attempts'>Attempt {this.state.camera.attempts} of 3</div> : ''
                                    }
                                    <button style={hideWhenCountingOrDisplayingInstructions} data-step={TIP} onClick={this.setStep} className='use'>
                                        <span data-step={TIP} className='chevron right'>Use photo</span>
                                    </button>
                                    <div style={displayInstructions} className='instructions'>
                                        <span>Fit your head within this shape</span>
                                    </div>
                                    <div style={displayWhenCounting} className='counter'>
                                        <span>{this.state.camera.count}</span>
                                    </div>
                                </main>
                            );
                        case TIP:
                            return (
                                <main className='photobooth tip'>
                                    <header>
                                        <img alt='' src={require('../images/csl-logo.png')} className='csl-logo'/>
                                    </header>
                                    <div className='contents'>
                                        <img alt='' src={require('../images/faces-logo.png')} className='faces-logo'/>
                                        <h1>Your photo has been saved.</h1>
                                        <h2>Please write your tip or advice in the box below:</h2>
                                        <form>
                                            <textarea maxlength="300" placeholder='Type message here' onChange={this.updateTip} value={this.props.delegate.get('tip')}></textarea>
                                        </form>
                                        <p className='terms'>Please note: Your tip should not mention any specific product or
                                            convey any positive or negative opinion on a company or their products. The tip
                                            is not intended to replace the advice and recommendations given by healthcare
                                            professionals.</p>
                                    </div>
                                    <footer>
                                        <a className={`cta-bottom ${tipDisabled ? 'disabled' : ''}`} data-step={CONSENT} onClick={this.setStep}>Save message</a>
                                        {/*<div onClick={this.skip} className='skip'><span className='chevron thick right'></span></div>*/}
                                    </footer>
                                </main>
                            );
                        case CONSENT:
                            return (
                                <main className='photobooth consent'>
                                    <header>
                                        <img alt='' src={require('../images/csl-logo.png')} className='csl-logo'/>
                                    </header>
                                    <div className='contents'>
                                        <img alt='' src={require('../images/faces-logo.png')} className='faces-logo'/>
                                        <h1>Almost done!<br/>Your message has been saved.</h1>
                                        <h2>Please complete your details below to confirm you are happy to share your tip and photograph on screen at this congress booth.</h2>
                                        <form>
                                            <p className='required'>*Required</p>
                                            <input type='text' onChange={this.updateFirst} value={this.props.delegate.get('first')} placeholder='First Name*'/>
                                            <input type='text' onChange={this.updateLast} value={this.props.delegate.get('last')} placeholder='Surname*'/>
                                            <input type='text' onChange={this.updateEmail} value={this.props.delegate.get('email')} placeholder='Email Address'/>
                                            <label htmlFor='contact-consent'>
                                                <input onClick={this.toggleContactConsent} id='contact-consent' type='checkbox'/>
                                                <span className='checkmark'></span>Please select if you are happy for CSL
                                                Behring to contact you if you make reference to an adverse event, for the
                                                purpose of obtaining additional information.</label>
                                            <h3 className='bold'>We would also like to contact you following today to share your photograph
                                                with you and provide additional information about the Faces of Hemophilia
                                                project.</h3>
                                            <label htmlFor='project-consent'>
                                                <input onClick={this.toggleProjectConsent} id='project-consent' type='checkbox'/>
                                                <span className='checkmark'></span>Please tick if you consent to CSL Behring contacting you for these reasons.</label>
                                            <h3 className='bold'>Your contact details will be stored securely and will not be shared with any
                                                third party without your prior consent.</h3>
                                            <input type='text' onChange={this.updateSignature} value={this.props.delegate.get('signature')} placeholder='Typed signature*'/>
                                            {/*<input type='text' onChange={this.updateDate} value={this.props.delegate.get('date')} placeholder='Date*'/>*/}
                                            <select onChange={this.updateDate} value={this.props.delegate.get('date')}>
                                                <option value='' disabled hidden>Please confirm today's date</option>
                                                <option value='today'>{moment().format('dddd, MMM Do YYYY')}</option>
                                            </select>
                                            <label className='bold' htmlFor='photo-consent'>
                                                <input onClick={this.togglePhotoConsent} id='photo-consent' type='checkbox'/>
                                                <span className='checkmark'></span>I hereby declare that I have voluntarily
                                                agreed to grant CSL Behring the right to use my photographic image and the
                                                advice that I have provided within this form. I hereby warrant that I am
                                                over 18 years and have the right to contract in my own name.<br/>I have read
                                                and understood the details above.*</label>
                                        </form>
                                    </div>
                                    <footer>
                                        <a className={`cta-bottom ${consentDisabled ? 'disabled' : ''}`} data-step={THANKS} onClick={this.setStep}>Consent & Submit</a>
                                    </footer>
                                </main>
                            );
                        case THANKS:
                            return (
                                <main className='photobooth thanks'>
                                    <header>
                                        <img alt='' src={require('../images/csl-logo.png')} className='csl-logo'/>
                                    </header>
                                    <div className='contents'>
                                        <img alt='' src={require('../images/faces-logo.png')} className='faces-logo'/>
                                        <h1 className='red'>Thank you.</h1>
                                        <p>Your submission has been received by our on-site team. </p>
                                        <p>Please check back later on the screens displayed on our stand<br/>to see your submission added to the Faces of Hemophilia.</p>
                                        <p className='red'>To support this valuable initiative, CSL Behring will make a<br/>donation to The Haemophilia Society UK.<br/>Thank you for your support.</p>
                                    </div>
                                    <p className='terms'>All messages must undergo review by our legal team to ensure regulatory compliance.<br/>Messages that do not pass legal review for any reason will not be displayed on stand. <br/><br/>UK/COAG/18-0046<br/>Date of preparation: April 2018</p>
                                </main>
                            );
                        default:
                            return <p>Welcome</p>;
                    }
                })()}
            </div>
        )
    }
}

function mapStateToProps( state ) {
    return {
        delegate: state.delegate
    };
}

function mapDispatchToProps( dispatch ) {
    return bindActionCreators( {
        updateDataURI: updateDataURI,
        updateTip: updateTip,
        updateFirst: updateFirst,
        updateLast: updateLast,
        updateEmail: updateEmail,
        updateContactConsent: updateContactConsent,
        updateProjectConsent: updateProjectConsent,
        updateSignature: updateSignature,
        updateDate: updateDate,
        updatePhotoConsent: updatePhotoConsent,
        createDelegate: createDelegate
    }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( PhotoBooth );