import React, { Component } from 'react';

class Busy extends Component {

    render() {

        const { show } = this.props;

        const display = show ? {display: 'block'} : {display: 'none'};

        return (
            <div style={display} className='modal'>
                <div className="spinner">
                    <div className="bounce1"></div>
                    <div className="bounce2"></div>
                    <div className="bounce3"></div>
                </div>
            </div>
        )
    }
}

export default Busy;