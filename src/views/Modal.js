import React, { Component } from 'react';

import {
    IMAGE_PATH,
    MODAL_IMAGE,
    MODAL_TEXT,
    MODAL_ERROR,
    MODAL_ALERT
} from './Constants';

class Modal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            message: '',
            id: 0
        };
        this.editMessage = this.editMessage.bind(this);
        this.edit = this.edit.bind(this);
        this.blockClick = this.blockClick.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.contents !== this.props.contents && this.props.mode === MODAL_TEXT) {
            this.setState({
                message: this.props.contents.get('message'),
                id: this.props.contents.get('id')
            })
        }
    }

    blockClick(e) {
        e.preventDefault();
        e.stopPropagation();
    }

    edit(e) {
        e.preventDefault();
        this.props.editMessage(e);
    }

    editMessage(e) {
        this.setState({
            message: e.target.value
        })
    }

    render() {

        const { show, close, mode, contents } = this.props;

        const display = show ? {display: 'block'} : {display: 'none'};

        const size = {
            width: mode === MODAL_TEXT || mode === MODAL_ERROR || mode === MODAL_ALERT ? '50%' : 'auto',
            height: 'auto',
            display: mode === MODAL_IMAGE ? 'table' : ''
        };

        const cont = mode === MODAL_TEXT ?
            <div className='edit-message'>
                <textarea onChange={this.editMessage} value={this.state.message}></textarea>
                <button data-id={this.state.id} data-message={this.state.message} onClick={this.edit} type="button" className="button">Submit</button>
            </div> :
                mode === MODAL_ERROR ?
                    <div className='error'>
                         <img src={require('../images/faces-logo.png')} alt='' />
                    </div> :
                    mode === MODAL_ALERT ?
                        <div className='info'>
                            <h3>Info</h3>
                            <h4>{contents}</h4>
                        </div> :
                            mode === MODAL_IMAGE ?
                                <img alt='' title={`${contents}`} src={`${IMAGE_PATH}${contents}`}/> : '';

        return (
            <div style={display} className='modal' onClick={close}>
                <div style={size} className='background'>
                    <div onClick={this.blockClick} className='contents'>
                        {cont}
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal;

//{/*onClick={close}*/}