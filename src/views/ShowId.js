import React, { Component } from 'react';

class ShowId extends Component {

    render() {

        const { ID } = this.props;

        const style = {
            'position' : 'relative',
            'color' : 'white',
            'fontSize' : '14px',
            'background' : 'black',
            'padding' : '0px 4px',
            'opacity' : '.5',
            'display' : 'none'
        };

        return (
            <span style={style}>{ID}</span>
        )
    }
}

export default ShowId;