import openSocket from 'socket.io-client';
import {
    IMAGE_READY,
    IMAGE_ERROR,
    NEW_APPROVAL,
    APPROVAL_REVOKED,
    DELEGATE_DELETED,
    TIP_UPDATED,
    SUBSCRIBE_TO_SOCKET,
    SOCKET_PATH
} from './Constants'

function subscribeToSocket(cb) {

    const socket = openSocket(SOCKET_PATH);

    socket.on('disconnect',()=> {
        console.log("// Lost connection to socket server, restart server and refresh client");
        socket.close();
    });

    socket.on('connect',()=> {
        console.log("// Client is connected to socket server");
        //socket.close();
        socket.emit(SUBSCRIBE_TO_SOCKET);
    });

    socket.on(IMAGE_READY, packet => cb(null, packet));
    socket.on(IMAGE_ERROR, packet => cb(null, packet));
    socket.on(NEW_APPROVAL, packet => cb(null, packet));
    socket.on(APPROVAL_REVOKED, packet => cb(null, packet));
    socket.on(DELEGATE_DELETED, packet => cb(null, packet));
    socket.on(TIP_UPDATED, packet => cb(null, packet));
    //socket.emit(SUBSCRIBE_TO_SOCKET);
}

export { subscribeToSocket }

// { console.log("// socket", packet); return cb(null, packet);});