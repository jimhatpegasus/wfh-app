import axios from 'axios';

import {
    API_PATH
} from "./views/Constants";

import {
    ACTION_CREATE_DELEGATE,
    ACTION_CREATE_DELEGATE_SUCCESS,
    ACTION_APPROVE_DELEGATE,
    ACTION_APPROVE_DELEGATE_SUCCESS,
    ACTION_DELETE_DELEGATE,
    ACTION_DELETE_DELEGATE_SUCCESS,
    ACTION_REJECT_DELEGATE,
    ACTION_REJECT_DELEGATE_SUCCESS,
    ACTION_RESET_DELEGATE,
    ACTION_RESET_DELEGATE_SUCCESS,
    ACTION_FETCH_ALL_DELEGATES,
    ACTION_FETCH_ALL_DELEGATES_SUCCESS,
    ACTION_FETCH_APPROVED_DELEGATES,
    ACTION_FETCH_APPROVED_DELEGATES_SUCCESS,
    ACTION_FETCH_LATEST_APPROVED_DELEGATES,
    ACTION_FETCH_LATEST_APPROVED_DELEGATES_SUCCESS,
    ACTION_FETCH_REJECTED_DELEGATES,
    ACTION_FETCH_REJECTED_DELEGATES_SUCCESS,
    ACTION_FETCH_PENDING_DELEGATES,
    ACTION_FETCH_PENDING_DELEGATES_SUCCESS,
    ACTION_API_ERROR,
    ACTION_PATCH_TIP,
    ACTION_PATCH_TIP_SUCCESS
} from "./actions/actions"

import {
    closeBusy,
    closeModal
} from "./actions/modalActions";

const dataService = store => next => action => {

    next(action);

    let params;

    switch (action.type) {

        case ACTION_APPROVE_DELEGATE:
            axios.patch(`${API_PATH}/approve/${action.data}`)
                .then(response => {
                    //if (response.data.error) {} else { store.dispatch(closeBusy()); }
                    response.data.error ?
                        next({ type: ACTION_API_ERROR, data: response.data }) :
                        next({ type: ACTION_APPROVE_DELEGATE_SUCCESS, data: response.data });
                })
                .catch(error => next({ type: ACTION_API_ERROR, data: error }));
            break;

        case ACTION_DELETE_DELEGATE:
            axios.delete(`${API_PATH}/delete/${action.data}`)
                .then(response => {
                    //if (response.data.error) {} else { store.dispatch(closeBusy()); }
                    response.data.error ?
                        next({ type: ACTION_API_ERROR, data: response.data }) :
                        next({ type: ACTION_DELETE_DELEGATE_SUCCESS, data: response.data });
                })
                .catch(error => next({ type: ACTION_API_ERROR, data: error }));
            break;

        case ACTION_REJECT_DELEGATE:
            axios.patch(`${API_PATH}/reject/${action.data}`)
                .then(response => {
                    //if (response.data.error) {} else { store.dispatch(closeBusy()); }
                    response.data.error ?
                        next({ type: ACTION_API_ERROR, data: response.data }) :
                        next({ type: ACTION_REJECT_DELEGATE_SUCCESS, data: response.data });
                })
                .catch(error => next({ type: ACTION_API_ERROR, data: error }));
            break;

        case ACTION_RESET_DELEGATE:
            axios.patch(`${API_PATH}/reset/${action.data}`)
                .then(response => {
                    //if (response.data.error) {} else { store.dispatch(closeBusy()); }
                    response.data.error ?
                        next({ type: ACTION_API_ERROR, data: response.data }) :
                        next({ type: ACTION_RESET_DELEGATE_SUCCESS, data: response.data });
                })
                .catch(error => next({ type: ACTION_API_ERROR, data: error }));
            break;

        case ACTION_PATCH_TIP:
            params = action.data;
            axios({
                method: 'post',
                url: `${API_PATH}/tip/`,
                data: {
                    id: params.delegate,
                    tip: params.tip
                }
            })
                .then(response => {
                    //if (response.data.error) {} else { store.dispatch(closeBusy()); }
                    if (response.data.error) {} else { store.dispatch(closeModal()); }
                    response.data.error ?
                        next({ type: ACTION_API_ERROR, data: response }) :
                        next({ type: ACTION_PATCH_TIP_SUCCESS, data: response.data });
                })
                .catch(error => next({ type: ACTION_API_ERROR, data: error }));
            break;

        case ACTION_FETCH_ALL_DELEGATES:
            axios
                .get(`${API_PATH}/`)
                .then(response => {
                    //if (response.data.error) {} else { store.dispatch(closeBusy()); }
                    response.data.error ?
                        next({ type: ACTION_API_ERROR, data: response.data }) :
                        next({ type: ACTION_FETCH_ALL_DELEGATES_SUCCESS, data: response.data });
                })
                .catch(error => next({ type: ACTION_API_ERROR, data: error }));
            break;

        case ACTION_FETCH_LATEST_APPROVED_DELEGATES:
            axios.get(`${API_PATH}/approved`)
                .then(response => {
                    if (response.data.error) {} else {
                        response.data.response = response.data.response.sort((a, b) => 0.5 - Math.random());
                    }
                    response.data.error ?
                        next({ type: ACTION_API_ERROR, data: response.data }) :
                        next({ type: ACTION_FETCH_LATEST_APPROVED_DELEGATES_SUCCESS, data: response.data });
                })
                .catch(error => next({ type: ACTION_API_ERROR, data: error }));
            break;

        case ACTION_FETCH_APPROVED_DELEGATES:
            axios.get(`${API_PATH}/approved`)
                .then(response => {
                    //if (response.data.error) {} else { store.dispatch(closeBusy()); }
                    response.data.error ?
                        next({ type: ACTION_API_ERROR, data: response.data }) :
                        next({ type: ACTION_FETCH_APPROVED_DELEGATES_SUCCESS, data: response.data });
                })
                .catch(error => next({ type: ACTION_API_ERROR, data: error }));
            break;

        case ACTION_FETCH_REJECTED_DELEGATES:
            axios.get(`${API_PATH}/rejected`)
                .then(response => {
                    //if (response.data.error) {} else { store.dispatch(closeBusy()); }
                    response.data.error ?
                        next({ type: ACTION_API_ERROR, data: response.data }) :
                        next({ type: ACTION_FETCH_REJECTED_DELEGATES_SUCCESS, data: response.data });
                })
                .catch(error => next({ type: ACTION_API_ERROR, data: error }));
            break;

        case ACTION_FETCH_PENDING_DELEGATES:
            axios.get(`${API_PATH}/pending`)
                .then(response => {
                    //if (response.data.error) {} else { store.dispatch(closeBusy()); }
                    response.data.error ?
                        next({ type: ACTION_API_ERROR, data: response.data }) :
                        next({ type: ACTION_FETCH_PENDING_DELEGATES_SUCCESS, data: response.data });
                })
                .catch(error => next({ type: ACTION_API_ERROR, data: error }));
            break;

        case ACTION_CREATE_DELEGATE:
            params = action.data;
            axios({
                method: 'post',
                url: `${API_PATH}/create/`,
                data: {
                    name: params.first,
                    last: params.last,
                    email: params.email,
                    message: params.tip,
                    file: params.dataURI,

                    contactConsent: params.contactConsent,
                    projectConsent: params.projectConsent,
                    photoConsent: params.photoConsent,
                    signature: params.signature

                }
            })
            .then(response => {
                //if (response.data.error) {} else { store.dispatch(closeBusy()); }
                response.data.error ?
                    next({ type: ACTION_API_ERROR, data: response }) :
                    next({ type: ACTION_CREATE_DELEGATE_SUCCESS, data: response.data });
            })
            .catch(error => next({ type: ACTION_API_ERROR, data: error }));
            break;

        default:
            break
    }

};

export default dataService