import * as actions from './actions';

export function approveDelegate( delegate ) {
    return {
        type: actions.ACTION_APPROVE_DELEGATE,
        data: delegate
    };
}

export function deleteDelegate( delegate ) {
    return {
        type: actions.ACTION_DELETE_DELEGATE,
        data: delegate
    };
}

export function rejectDelegate( delegate ) {
    return {
        type: actions.ACTION_REJECT_DELEGATE,
        data: delegate
    };
}

export function resetDelegate( delegate ) {
    return {
        type: actions.ACTION_RESET_DELEGATE,
        data: delegate
    };
}

export function updateTip( delegate, tip ) {
    return {
        type: actions.ACTION_PATCH_TIP,
        data: {
            'delegate' : delegate,
            'tip' : tip
        }
    };
}

export function fetchAllDelegates() {
    return {
        type: actions.ACTION_FETCH_ALL_DELEGATES,
        data: null
    };
}

export function fetchLatestApprovedDelegates() {
    return {
        type: actions.ACTION_FETCH_LATEST_APPROVED_DELEGATES,
        data: 18
    };
}

export function fetchApprovedDelegates() {
    return {
        type: actions.ACTION_FETCH_APPROVED_DELEGATES,
        data: null
    };
}

export function fetchRejectedDelegates() {
    return {
        type: actions.ACTION_FETCH_REJECTED_DELEGATES,
        data: null
    };
}

export function fetchPendingDelegates() {
    return {
        type: actions.ACTION_FETCH_PENDING_DELEGATES,
        data: null
    };
}

export function createDelegate( delegate ) {
    return {
        type: actions.ACTION_CREATE_DELEGATE,
        data: delegate
    };
}

export default createDelegate;
