import * as actions from './actions';

export function openModal( modal ) {
    return {
        type: actions.ACTION_OPEN_MODAL,
        data: modal
    };
}

export function closeModal() {
    return {
        type: actions.ACTION_CLOSE_MODAL,
        data: null
    };
}

export function openBusy() {
    return {
        type: actions.ACTION_OPEN_BUSY,
        data: null
    };
}

export function closeBusy() {
    return {
        type: actions.ACTION_CLOSE_BUSY,
        data: null
    };
}