import * as actions from './actions';

export function updateDataURI( dataURI ) {
    return {
        type: actions.ACTION_UPDATE_DATAURI,
        data: dataURI
    };
}

export function updateTip( tip ) {
    return {
        type: actions.ACTION_UPDATE_TIP,
        data: tip
    };
}

export function updateFirst( first ) {
    return {
        type: actions.ACTION_UPDATE_FIRST,
        data: first
    };
}

export function updateLast( last ) {
    return {
        type: actions.ACTION_UPDATE_LAST,
        data: last
    };
}

export function updateEmail( email ) {
    return {
        type: actions.ACTION_UPDATE_EMAIL,
        data: email
    };
}

export function updateContactConsent( consent) {
    return {
        type: actions.ACTION_UPDATE_CONTACT_CONSENT,
        data: consent
    };
}

export function updateProjectConsent( consent ) {
    return {
        type: actions.ACTION_UPDATE_PROJECT_CONSENT,
        data: consent
    };
}

export function updateSignature( signature ) {
    return {
        type: actions.ACTION_UPDATE_SIGNATURE,
        data: signature
    };
}

export function updateDate( date ) {
    return {
        type: actions.ACTION_UPDATE_DATE,
        data: date
    };
}

export function updatePhotoConsent( consent ) {
    return {
        type: actions.ACTION_UPDATE_PHOTO_CONSENT,
        data: consent
    };
}

export default updateDataURI;
